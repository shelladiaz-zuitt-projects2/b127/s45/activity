//for use state
import { useState } from 'react';
import { Row, Col, Card, Button } from 'react-bootstrap';
//used proptype check if components is getting the correct prop type
//prop types user for validating information passed to a component and is a tool normally used to help developers ensure the correct information is passed from one component to the next
import PropTypes from 'prop-types';

//1st way - export default function CourseCard(props){
    //deconstructuring method
    export default function CourseCard({courseProp}){
    //check to see if successfullt passed
    // console.log(props)
    // console.log(typeof props)
    //2nd way ; destructuring part 2 para aalisin si courseProp, idedefine na
    const { name, description, price } = courseProp;

    //use the state hook for this component to be able to store its state
    //state are used to keep track of information related to individual component
    //syntax
        //const[getter, setter] useState(initialGetterValue)
        //getter = inital state or default value
        //setter = updated value = (initialGetterValue)
    const[count, setCount] = useState(0)

    console.log(useState(0))

    function enroll(){
        setCount(count + 1);
        console.log('Enrollees:' + count);
    }


    return(
        <Row>
            <Col xs={12} md={12}  className="mt-3">
                <Card className="cardCourse p-3">
                    <Card.Body>
                        <Card.Title>
                        {/* 1st way  */}
                            {/* <h2>{props.courseProp.name} </h2> */}
                          {/* 2nd way  */}   
                            {/* <h2>{courseProp.name} </h2> */}
                            <h2>{name} </h2>

                        </Card.Title>
                        <Card.Subtitle>
                            Description:
                        </Card.Subtitle>
                        <Card.Text>
                        {/* 1st way  */}
                        {/* {props.courseProp.description} */}
                        {/* 2nd way  */}   
                        {/* {courseProp.description} */}
                        {description}


                        </Card.Text>
                        <Card.Subtitle>
                            Price:
                        </Card.Subtitle>
                        <Card.Text>
                        {/* 1st way  */}
                            {/* &#8369; {props.courseProp.price} */}
                        {/* 2nd way  */}   
    
                            {/* &#8369; {courseProp.price} */}
                            &#8369; {price}


                        </Card.Text>
                        <Card.Text>
                        {/* //kunin si getter */}
                            Enrollees: {count}
                        </Card.Text>

                        <Button variant="primary" onClick={enroll}>Enroll</Button>
                    </Card.Body>
                </Card>
                
            </Col>
        </Row>
    )
}

//used proptype check if components is getting the correct prop type
//prop types user for validating information passed to a component and is a tool normally used to help developers ensure the correct information is passed from one component to the next

CourseCard.propTypes = {
    //'shape' method is used to check if a prop object conforms to a specific 'shape
    courseProp: PropTypes.shape({
        //define the properties and their expected types
        name: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired
    })
}
