import { Fragment  } from 'react';
import courseData from '../data/courseData';
import CourseCard from '../components/CourseCard';



export default function Courses(){
    //checls to see if the mock data was captured
    console.log(courseData)
    console.log(courseData[0])

    //para madispay lahat ng courses gagamit ng map method.
    //map() method loops through the individual course objects in our array and returns a component for each course

    const courses = courseData.map(course => {
        return(
            //naglagay ng key para maging unique para walang error that help React js identify which /elements have been change, added or removed
            //to keep track the data of courses
            <CourseCard key={course.id} courseProp={course}/>
        )
    })

    return(
        <Fragment>
            <h1>Courses</h1>
            {courses}
        </Fragment>
  
    )
}